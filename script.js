//Variables-----------------------------------------------------------------
let modal = document.getElementById("new-student-modal-window");
let warning = document.getElementById("student-deletion-warning");
let btn = document.getElementById("add-student-button");
let closeButton = document.getElementsByClassName("close-button");
let table = document.getElementById("table-of-students");
let mainCheckbox = document.getElementById("main-checkbox");
let creationForm = document.getElementById("creating-student-form");
let submitButton = document.getElementById("submit-student");
let modalWindowHeader = document.getElementById("modal-window-header");
let rowIndex;

//Animation-----------------------------------------------------------------
function animateNotification(){
    const element = document.querySelector('#notifications')

    element.style.transitionDuration = "0.7s"
    element.style.content = " ";
    element.style.position = "absolute";
    element.style.top = "14px";
    element.style.right = "17px";
    element.style.width = "10px";
    element.style.height = "10px";
    element.style.borderRadius = "50%";
    element.style.background = "orangered";
}

//Modal windows------------------------------------------------------------------

function showWarning (r){
    rowIndex = r.parentNode.parentNode.parentNode.rowIndex;
    let warningText = "Are you sure you want to delete user " + document.getElementById("table-of-students").rows[rowIndex].cells[2].innerText + "?";
    document.getElementById("warning-text").innerText = warningText;
    warning.style.display = "block";
}

function showEdit (r){
    rowIndex = r.parentNode.parentNode.parentNode.rowIndex;
    let studentToEdit = studentsArray[rowIndex - 1]

    let groupOptions = document.getElementById("group").options;
    for(let i = 0; i < groupOptions.length; i++){
        if(groupOptions[i].text === studentToEdit.Group){
            document.getElementById("group").selectedIndex = i;
        }
    }

    let genderOptions = document.getElementById("gender").options;
    for(let i = 0; i < genderOptions.length; i++){
        if(genderOptions[i].text === studentToEdit.Gender){
            document.getElementById("gender").selectedIndex = i;
        }
    }

    document.getElementById("first-name").value = studentToEdit.FirstName;
    document.getElementById("last-name").value= studentToEdit.LastName;

    let dateParts = studentToEdit.Birthday.split(".");
    let dateObject = new Date(`${dateParts[2]}-${dateParts[1]}-${dateParts[0]}`);
    document.getElementById("birthday").value = dateObject.toISOString().slice(0,10);

    document.getElementById("new-student-modal-window").style.display = "block";
    submitButton.value = "Save";
    modalWindowHeader.innerHTML = "Edit student";
}

function closeWarning(){
    warning.style.display = "none";
}

btn.onclick = function() {
    modal.style.display = "block";
}

function closeWindow(){
    modal.style.display = "none";
    document.getElementById("creating-student-form").reset();
}

for (let i = 0, len = closeButton.length; i < len; i++){
    closeButton[i].onclick = closeWindow;
}

window.onclick = function(event) {
    if (event.target === modal || event.target === warning) {
        modal.style.display = "none";
        warning.style.display = "none";
        document.getElementById("creating-student-form").reset();
    }
}

//Checkbox--------------------------------------------------------------------------------

$(document).on("click", ".student-checkbox", function () {
    mainCheckbox.checked = $(".student-checkbox:checked").length === $(".student-checkbox").length;
})

mainCheckbox.onclick = function (){
    let newStatus = false;
    if (mainCheckbox.checked === true){
        newStatus = true;
    }

    const checkBoxes = document.getElementsByClassName("student-checkbox");
    for (let i = 0; i < checkBoxes.length; i++){
        checkBoxes[i].checked = newStatus;
    }
}

//Working with data-------------------------------------------------

const student = {
    Group : "",
    FirstName: "",
    LastName: "",
    Gender: "",
    Birthday: "",
    Id: "",
}

let studentsArray = [];

function deleteStudent() {
    let studentToDelete = studentsArray[rowIndex - 1];
    let dataToSend = {
        query: "delete",
        id: studentToDelete.Id
    };


    fetch('server.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataToSend)
    })
        .then(response => response.json())
        .then(data => {
            if (data.status === false || data.error != null) {
                swal(data.result, data.error, "error");
                document.getElementById("creating-student-form").reset();
                closeWarning();
            }
            else {
                console.log(data.result);
                document.getElementById('table-of-students').deleteRow(rowIndex);
                studentsArray.splice(rowIndex - 1, 1)
                closeWarning();
            }
        })
        .catch(error => {
            console.log('Error:', error);
            closeWarning();
            swal("Oops!", "Something went wrong", "error");
        });
}

creationForm.addEventListener("submit", (event) => {
    event.preventDefault();

    let newStudent, dataToSend;

    if (submitButton.value === "Create"){
        newStudent = structuredClone(student);
        newStudent.Group = $("#group option:selected").text();
        newStudent.FirstName = $("#first-name").val();
        newStudent.LastName = $("#last-name").val();
        newStudent.Gender = $("#gender option:selected").text();
        let date = new Date($('#birthday').val());
        newStudent.Birthday = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 )) .toISOString() .split("T")[0];

        dataToSend = {
            query: "create",
            group: newStudent.Group,
            firstName: newStudent.FirstName,
            lastName: newStudent.LastName,
            gender: newStudent.Gender,
            birthday: newStudent.Birthday,
            id: 0,
        };
    }
    else if (submitButton.value === "Save"){
        newStudent = studentsArray[rowIndex - 1]

        newStudent.Group = $("#group option:selected").text();
        newStudent.FirstName = $("#first-name").val();
        newStudent.LastName = $("#last-name").val();
        newStudent.Gender = $("#gender option:selected").text();
        let date = new Date($('#birthday').val());
        newStudent.Birthday = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 )) .toISOString() .split("T")[0];

        dataToSend = {
            query: "save",
            group: newStudent.Group,
            firstName: newStudent.FirstName,
            lastName: newStudent.LastName,
            gender: newStudent.Gender,
            birthday: newStudent.Birthday,
            id: newStudent.Id
        };
    }

    console.log(JSON.stringify(dataToSend));

    fetch('server.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataToSend)
    })
        .then(response => {
            return response.json()
        })
        .then(data => {
            if (data.status === false || data.error != null) {
                swal("Error code:" + data.error.code, data.error.message, "error");
                document.getElementById("creating-student-form").reset();
                closeWindow();
            } else {
                if (submitButton.value === "Create") {
                    let newStudent = makeStudent(data);
                    studentsArray.push(newStudent);
                    addStudentToList(newStudent);
                } else {
                    let newStudent = makeStudent(data);
                    closeWindow();
                    studentsArray[rowIndex - 1] = newStudent;
                    let row = table.rows[rowIndex];
                    row.cells[1].innerText = newStudent.Group;
                    row.cells[2].innerText = newStudent.FirstName + " " + newStudent.LastName;
                    row.cells[3].innerText = newStudent.Gender;
                    row.cells[4].innerText = newStudent.Birthday;
                }
                document.getElementById("creating-student-form").reset();
            }
        })
        .catch(error => {
            console.log('Error:', error);
            document.getElementById("creating-student-form").reset();
            closeWindow();
            swal("Oops!", "Something went wrong", "error");
        });
})

//Inserting record-----------------------------------------------------------------------------------

function addStudentToList (student){
    closeWindow();

    let row = table.insertRow();
    let checkBoxCell = row.insertCell(0)
    let GroupCell = row.insertCell(1)
    let NameCell = row.insertCell(2)
    let GenderCell = row.insertCell(3)
    let BirthdayCell = row.insertCell(4)
    let StatusCell = row.insertCell(5)
    let OptionsCell = row.insertCell(6)

    checkBoxCell.innerHTML = "<input type=\"checkbox\" class=\"student-checkbox\">"
    GroupCell.innerHTML = student.Group
    NameCell.innerHTML = student.FirstName + " " + student.LastName
    GenderCell.innerHTML = student.Gender
    BirthdayCell.innerHTML = student.Birthday.replaceAll("-", ".")
    StatusCell.innerHTML = "<div class=\"studentStatus-indicator\"></div>"
    OptionsCell.innerHTML = "<div>\n" +
        "                                <button style=\"bottom: 0; right: 40px\" class=\"btn\" onclick=\"showEdit (this)\"><i class=\"fa fa-pencil\" style=\"color: black\"></i></button>\n" +
        "                                <button id=\"delete-student-record\" style=\"bottom: 0; right: 40px\" class=\"btn\" onclick=\"showWarning(this)\"><i class=\"fa fa-close delete-button\" style=\"color: black\"></i></button>\n" +
        "                            </div>"
}

//Fetching database data at startup
window.onload = function (){
    fetch('retrieve-data.php')
        .then(response => response.json())
        .then(data => {
            console.log(data);
            for (let i = 0; i < data.length; i++){
                let newStudent = makeStudent(data[i]);
                addStudentToList(newStudent);
                studentsArray.push(newStudent)
            }
        })
        .catch(error => {
            console.error('Error fetching data:', error);
        });
}

//Making student object locally
function makeStudent(studentJson){
    let studentRecord = structuredClone(student);
    studentRecord.FirstName = studentJson.name;
    studentRecord.LastName = studentJson.surname;
    studentRecord.Group = studentJson.group;
    studentRecord.Gender = studentJson.gender;
    studentRecord.Birthday = studentJson.birthday;
    studentRecord.Id = studentJson.id;
    return studentRecord;
}

// + serviceWorker
if (navigator.serviceWorker) {
    navigator.serviceWorker.register('./service-worker.js')
        .then(() => {
            console.log('ServiceWorker registration successful')
        })
        .catch(err => {
            console.log('ServiceWorker registration failed', err)
        })
}

// Transitions between messages and table
let messagesAreOpened = false
let firstExecution = true;
let tableDiv, tableDivContent, messagesDiv, messagesDivContent
function openMessages(){
    if(!messagesAreOpened){
        tableDiv = document.getElementById("body-table-frame")
        tableDivContent = tableDiv.innerHTML
        if(firstExecution){
            $('#body-table-frame').load('chat.html #chat-interface')
            firstExecution = false
        }
        else {
            document.getElementById("body-table-frame").innerHTML = messagesDivContent
        }

        document.querySelector(".footer").style.display = 'none';
        messagesAreOpened = true;
    }
}

function openTable(){
    if(messagesAreOpened){
        messagesDiv = document.getElementById("body-table-frame")
        messagesDivContent = messagesDiv.innerHTML
        document.getElementById("body-table-frame").innerHTML = tableDivContent
        document.querySelector(".footer").style.display = 'block';
        messagesAreOpened = false;
    }
}
