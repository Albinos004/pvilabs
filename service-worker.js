const CACHE_NAME = 'cache'

self.addEventListener('install', ev => {
  ev.waitUntil(
    caches
        .open(CACHE_NAME)
        .then(async cache => {
            const filesToCache = [
                'index.html',
                'script.js',
                'studentStyles.css'
            ]

            console.log('service-worker started, caching ' + filesToCache)
            
            try {
                await cache.addAll(filesToCache)
            } catch (error) {
                console.log('cache not added')
            }
    })) 

    console.log('service-worker installed')
})