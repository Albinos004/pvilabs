<?php
$conn = ConnectDatabase();

$jsonData = 0;

if(isset($_POST)){
    $data = file_get_contents('php://input');
    $jsonData = json_decode($data);


    if (!$jsonData || !isset($jsonData->query)) {
        $response = array(
            'status' => false,
            'error' => array(
                'code' => 505,
                'message' => "Internal server error"
            )
        );

        echo json_encode($response);;
        return;
    }

    if ($jsonData->query == "delete"){
        $item_id = $jsonData->id;
        $sql = "DELETE FROM studentstb WHERE id = $item_id";

        $response = 0;

        if ($conn->query($sql) === TRUE) {
            $response = array(
                'status' => true,
                'error' => null,
                'result' => "Item deleted successfully"
            );
        } else {
            $response = array(
                'status' => false,
                'error' => $conn->error,
                'result' => "Error. Could not delete item"
            );
        }

        echo json_encode($response);

        $conn -> close();
        return;
    }

    $firstName = $jsonData->firstName;
    $lastName = $jsonData->lastName;
    $group = $jsonData->group;
    $gender = $jsonData->gender;
    $birthday = $jsonData->birthday;


    if(!isset($group) || !isset($firstName) || !isset($lastName) ||
        !isset($gender) || !isset($birthday)){

        $response = array(
            'status' => false,
            'error' => array(
                'code' => 500,
                'message' => "Internal server error"
            )
        );

        $json = json_encode($response);


        echo $json;
        return;
    }

    if ($jsonData->query == "create"){
        $sql = "INSERT INTO studentstb (`name`, `surname`, `group`, `gender`, `birthday`)
        VALUES ('$firstName', '$lastName', '$group', '$gender', '$birthday')";

        $response = 0;

        if ($conn->query($sql) === TRUE) {
            $last_inserted_id = mysqli_insert_id($conn);
            $response = array(
                'status' => true,
                'error' => null,
                'group' => $group,
                'name' => $firstName,
                'surname' => $lastName,
                'gender' => $gender,
                'birthday' => $birthday,
                'id' => $last_inserted_id
            );
        }
        else {
            $response = array(
                'status' => false,
                'error' => 123,
                'id' => 1
            );
        }

        echo json_encode($response);;
    }
    else if ($jsonData->query == "save"){
        $id = $jsonData->id;

        $sql = "UPDATE studentstb
        SET `name`='$firstName', `surname`='$lastName', `group`='$group', `gender`='$gender', `birthday`='$birthday'
        WHERE `id`='$id'";

        $response = 0;

        if ($conn->query($sql) === TRUE) {
            $response = array(
                'status' => true,
                'error' => null,
                'group' => $group,
                'name' => $firstName,
                'surname' => $lastName,
                'gender' => $gender,
                'birthday' => $birthday,
                'id' => $id
            );
        }
        else {
            $response = array(
                'status' => false,
                'error' => 123,
                'id' => $id
            );
        }

        echo json_encode($response);
    }


    $conn -> close();
    return;
}


function ConnectDatabase(){
    $servername = "localhost";
    $username = "root";
    $password = "Vs22041967";
    $database = "students";

// Create connection
    $conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    return $conn;
}