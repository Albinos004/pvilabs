const socket = io('http://localhost:3000');

let room
let inRoom = false

socket.on('roomUsers', users => {
    outputUsers(users);
});

socket.on('roomUsers', ({ room, users }) => {
    outputUsers(users);
});

socket.on("historyMessage", (message) => {
    addMessage(message.username, message.time, message.text);

    const chatMessages = document.getElementById("messages")
    chatMessages.scrollTop = chatMessages.scrollHeight;
})

socket.on("createDirectMessageRoom", username => {
    createChatRoomPreview(username)
})

socket.on('message', (message) => {
    console.log(message);
    addMessage(message.username, message.time, message.text);

    const chatMessages = document.getElementById("messages")
    chatMessages.scrollTop = chatMessages.scrollHeight;
})

function joinRoom (element) {
    document.getElementById("members").innerHTML = ''
    document.getElementById("messages").innerHTML = ''

    if (inRoom){
        socket.emit('leaveCurrentRoom')
    }

    room = element.innerText;

    socket.emit('joinRoom', room)
    inRoom = true
}

function joinDirect (element) {
    document.getElementById("members").innerHTML = ''
    document.getElementById("messages").innerHTML = ''

    if (inRoom){
        socket.emit('leaveCurrentRoom')
    }

    room = element.innerText;

    socket.emit('joinDirect', room)
    inRoom = true
}
function addUser(userName){
    const div = document.createElement("div")
    div.classList.add("user-nickname-div")
    const span = document.createElement('span')
    span.classList.add('user-nickname-span')
    span.innerText = `${userName}`
    div.appendChild(span)
    return div;
}

function addMessage(sender, time, text){
    const messageDiv = document.createElement("div")
    messageDiv.classList.add("message")

    const messageInfoDiv = document.createElement("div")
    messageInfoDiv.classList.add("message-info")
    const spanSender = document.createElement("span")
    spanSender.classList.add("message-sender")
    spanSender.innerText = `${sender}`
    const spanTime = document.createElement("span")
    spanTime.classList.add("message-time")
    spanTime.innerText = `${time}`

    messageInfoDiv.appendChild(spanSender)
    messageInfoDiv.appendChild(spanTime)
    messageDiv.appendChild(messageInfoDiv)

    const pContent = document.createElement("p")
    pContent.classList.add("message-content")
    pContent.innerText = `${text}`

    messageDiv.appendChild(pContent)

    document.getElementById("messages").appendChild(messageDiv)
}

function outputUsers(users){
    const div = document.querySelector("#members")
    div.innerHTML = ""

    for (let i = 0; i < users.length; i++) {
        const user = users[i];
        const newUser = addUser(user.username);
        div.appendChild(newUser);
    }
}

function SendMessage(){
    const inputLine = document.getElementById("text-input")
    let msg = inputLine.value;
    console.log(msg)

    msg = msg.trim();

    if (!msg) {
        return false;
    }

    socket.emit('chatMessage', msg);

    inputLine.value = '';
    inputLine.focus();
}

function Login(){
    let userName = document.getElementById("nickname").value
    document.getElementById("nickname").value = ""

    userName = userName.trim();

    if (!userName) {
        return false;
    }

    document.getElementById("login-button").disabled = true;
    document.getElementById("current-username").innerText = "You: " + userName

    //socket.emit("newUserLogin")

    socket.emit("login", userName)
}

function createChatRoomPreview(roomName) {
    const div = document.createElement("div");
    div.classList.add("chat-room-preview");
    div.setAttribute("onclick", "joinDirect(this)");

    const span = document.createElement("span");
    span.classList.add("room-title");
    span.innerText = roomName;

    div.appendChild(span);

    document.getElementById("chat-rooms").appendChild(div);
}
